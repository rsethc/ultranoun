use std::env::args;

struct CommonChar {
    ch: char,
    min_offset: usize,
    a_pos: usize,
    b_pos: usize,
}

fn ultrify (mut a: String, mut b: String) -> String {
    let mut ultra = "".to_string();
    while a.len() > 0 && b.len() > 0 {
        // Find earliest shared character
        let mut common : Option<CommonChar> = None;

        for (a_i, a_ch) in a.chars().enumerate() {
            for (b_i, b_ch) in b.chars().enumerate() {
                if a_ch == b_ch {
                    let min_offset = a_i.min(b_i);
                    let better = common.as_ref().map(|best| min_offset < best.min_offset).unwrap_or(true);
                    if better {
                        common = Some(CommonChar {
                            ch: a_ch,
                            min_offset,
                            a_pos: a_i,
                            b_pos: b_i,
                        });
                    }
                }
            }
        }
        if let Some(common) = common {
            ultra += &a[..common.a_pos];
            a = a[common.a_pos + 1 ..].to_string();
            ultra += &b[..common.b_pos];
            b = b[common.b_pos + 1 ..].to_string();
            ultra += &common.ch.to_string();
        } else {
            break;
        }
    }

    ultra + &a + &b
}

fn main() {
    let mut words : Vec<String> = args().skip(1).collect();

    while words.len() > 1 {
        let a = words.pop().unwrap();
        let b = words.pop().unwrap();
        words.push(ultrify(a,b));
    }
    println!("{}", words.pop().unwrap_or_default());
}
