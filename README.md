Silly utility for combining words together.

Example:

```
$ cargo run -- fernbank after dark
    Finished dev [unoptimized + debuginfo] target(s) in 0.00s
     Running `target/debug/ultranoun fernbank after dark`
dafternbank
```